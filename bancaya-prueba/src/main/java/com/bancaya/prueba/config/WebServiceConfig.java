package com.bancaya.prueba.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import com.bancaya.prueba.component.PokemonEndpointInterceptor;

/**
 * Configuracion de los servicios web
 * @author jesus
 *
 */
@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
	
	@Autowired 
	private PokemonEndpointInterceptor pokemonEndpointInterceptor;
	
	/**
	 * Metodo que registra el endpoint ws para las peticiones SOAP
	 * @param applicationContext
	 * @return
	 */
	@Bean
	public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean<>(servlet, "/ws/*");
	}

	/**
	 * Bean que define el esquema y xsd para el SOAP de Pokemon
	 * @param pokemonSchema
	 * @return
	 */
	@Bean(name = "pokemon")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema pokemonSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("PokemonPort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setTargetNamespace("http://spring.io/guides/gs-producing-web-service");
		wsdl11Definition.setSchema(pokemonSchema);
		return wsdl11Definition;
	}

	/**
	 * Bean que regresa el schema de pokemon
	 * @return
	 */
	@Bean
	public XsdSchema pokemonSchema() {
		return new SimpleXsdSchema(new ClassPathResource("pokemon.xsd"));
	}
	
	/**
	 * Metodo que agrega el interceptor para las peticiones SOAP
	 */
	@Override
    public void addInterceptors(List<EndpointInterceptor> interceptors) {
		interceptors.add(pokemonEndpointInterceptor);
    }
	
}