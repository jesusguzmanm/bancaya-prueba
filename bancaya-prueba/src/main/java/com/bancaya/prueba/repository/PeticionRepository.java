package com.bancaya.prueba.repository;

import org.springframework.data.repository.CrudRepository;

import com.bancaya.prueba.model.Peticion;

/**
 * Repositorio para guardar y consultar las peticiones
 * @author jesus
 *
 */
public interface PeticionRepository extends CrudRepository<Peticion, Long>{

}
