package com.bancaya.prueba.endpoint;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bancaya.prueba.model.rest.Ability;
import com.bancaya.prueba.model.rest.Item;
import com.bancaya.prueba.model.rest.Pokemon;
import com.bancaya.prueba.model.rest.encounter.LocationArea;
import com.bancaya.prueba.service.IPokemonService;
import com.bancaya.prueba.utils.PokemonUtils;

import io.spring.guides.gs_producing_web_service.Abilities;
import io.spring.guides.gs_producing_web_service.AbilitiesResponse;
import io.spring.guides.gs_producing_web_service.BaseExperience;
import io.spring.guides.gs_producing_web_service.BaseExperienceResponse;
import io.spring.guides.gs_producing_web_service.HeldItems;
import io.spring.guides.gs_producing_web_service.HeldItemsResponse;
import io.spring.guides.gs_producing_web_service.Id;
import io.spring.guides.gs_producing_web_service.IdResponse;
import io.spring.guides.gs_producing_web_service.LocationAreaEncounters;
import io.spring.guides.gs_producing_web_service.LocationAreaEncountersResponse;
import io.spring.guides.gs_producing_web_service.Name;
import io.spring.guides.gs_producing_web_service.NameResponse;

/**
 * Endpoint para las peticiones SOAP
 * @author jesus
 *
 */
@Endpoint
public class PokemonEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	
	@Autowired
	private IPokemonService pokemonService;
	
	/**
	 * Metodo que obtiene las habilidades
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "abilities")
	@ResponsePayload
	public AbilitiesResponse getAbilities(@RequestPayload Abilities request) {
		AbilitiesResponse response = new AbilitiesResponse();
		Pokemon p = findPokemonByName(request.getNombre());
		for(Ability a : p.getAbilities()) {
			response.getAbilities().add(PokemonUtils.abilityToSOAP(a));
		}
		return response;
	}
	
	/**
	 * Metodo que obtiene las experiencia base
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "base_experience")
	@ResponsePayload
	public BaseExperienceResponse getBaseExperience(@RequestPayload BaseExperience request) {
		BaseExperienceResponse response = new BaseExperienceResponse();
		Pokemon p = findPokemonByName(request.getNombre());
		response.setBaseExperience(BigInteger.valueOf(p.getBaseExperience()));
		return response;
	}
	
	/**
	 * Metodo que obtiene los items que puede tener el pokemon
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "held_items")
	@ResponsePayload
	public HeldItemsResponse getHeldItems(@RequestPayload HeldItems request) {
		HeldItemsResponse response = new HeldItemsResponse();
		Pokemon p = findPokemonByName(request.getNombre());
		for(Item i : p.getHeldItems()) {
			response.getItem().add(PokemonUtils.itemToSOAP(i));
		}
		return response;
	}
	
	/**
	 * Metodo que obtiene el id del pokemon
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "id")
	@ResponsePayload
	public IdResponse getId(@RequestPayload Id request) {
		IdResponse response = new IdResponse();
		Pokemon p = findPokemonByName(request.getNombre());
		response.setId(BigInteger.valueOf(p.getId()));
		return response;
	}
	
	/**
	 * Metodo que obtiene el nombre del pokemon
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "name")
	@ResponsePayload
	public NameResponse getName(@RequestPayload Name request) {
		NameResponse response = new NameResponse();
		Pokemon p = findPokemonByName(request.getNombre());
		response.setName(p.getName());
		return response;
	}
	
	/**
	 * Metodo que obtiene las locaciones donde aparece el pokemon
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "location_area_encounters")
	@ResponsePayload
	public LocationAreaEncountersResponse getLocationAreaEncounters(@RequestPayload LocationAreaEncounters request) {
		LocationAreaEncountersResponse response = new LocationAreaEncountersResponse();
		List<LocationArea> lal = pokemonService.getLocationAreaEncounters(request.getNombre());
		for(LocationArea la : lal) {
			response.getLocationAreaEncounters().add(PokemonUtils.locationAreaToSOAP(la));
		}
		return response;
	}
	
	private Pokemon findPokemonByName(String nombre) {
		return pokemonService.getPokemon(nombre);
	}
}
