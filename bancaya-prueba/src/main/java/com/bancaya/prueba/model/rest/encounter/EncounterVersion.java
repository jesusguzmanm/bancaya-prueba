package com.bancaya.prueba.model.rest.encounter;

import java.util.List;

import com.bancaya.prueba.model.rest.NameAndUrl;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Representacion de la version del encuentro
 * @author jesus
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
@ToString
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EncounterVersion {
	@Getter @Setter
	private Integer maxChance;
	@Getter @Setter
	private NameAndUrl version;
	@Getter @Setter
	private List<EncounterDetail> encounterDetails;
}
