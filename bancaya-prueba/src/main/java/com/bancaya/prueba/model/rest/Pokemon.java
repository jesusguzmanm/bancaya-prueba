package com.bancaya.prueba.model.rest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Representacion raiz del pokemon
 * @author jesus
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
@ToString
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Pokemon {
	
	@Getter @Setter
	private List<Ability> abilities;
	@Getter @Setter
	private Long baseExperience;
	@Getter @Setter
	private List<Item> heldItems;
	@Getter @Setter
	private Integer id;
	@Getter @Setter
	private String name;
	
}
