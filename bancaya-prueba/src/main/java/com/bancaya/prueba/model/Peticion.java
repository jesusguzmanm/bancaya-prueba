package com.bancaya.prueba.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@EqualsAndHashCode
@EntityListeners(AuditingEntityListener.class)
@ToString
public class Peticion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String ipOrigen;
	
	@Getter @Setter
	@CreatedDate
	private Date fechaRequest;
	
	@Getter @Setter
	@Enumerated(EnumType.STRING)
	private Metodo metodo;
	
}
