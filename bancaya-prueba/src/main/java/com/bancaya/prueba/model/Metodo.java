package com.bancaya.prueba.model;

import java.util.HashMap;
import java.util.Map;

public enum Metodo {
	ABILITIES("getAbilities"),
	BASE_EXPERIENCE("getBaseExperience"),
	HELD_ITEMS("getHeldItems"),
	ID("getId"),
	NAME("getName"),
	LOCATION_AREA_ENCOUNTERS("getLocationAreaEncounters"),
	UNKNOWN("none");
	
	private String nombre;
	
	Metodo(String nombre){
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

    private static final Map<String, Metodo> lookup = new HashMap<>();

    static
    {
        for(Metodo env : Metodo.values())
        {
            lookup.put(env.getNombre(), env);
        }
    }
  
    public static Metodo get(String nombre) 
    {
    	if(lookup.containsKey(nombre)) {
    		return lookup.get(nombre);
    	} 
    	return UNKNOWN;
    }
}
