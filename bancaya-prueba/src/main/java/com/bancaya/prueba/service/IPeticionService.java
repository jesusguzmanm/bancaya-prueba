package com.bancaya.prueba.service;

import com.bancaya.prueba.model.Metodo;
import com.bancaya.prueba.model.Peticion;

/**
 * Interface para el servicio de las peticiones
 * @author jesus
 *
 */
public interface IPeticionService {

	/**
	 * Metodo que guarda una peticion
	 * @param metodo
	 * @return
	 */
	Peticion guardarPeticion(Metodo metodo);
	
}
