package com.bancaya.prueba.service;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bancaya.prueba.model.rest.Pokemon;
import com.bancaya.prueba.model.rest.encounter.LocationArea;

@Service
public class PokemonServiceImpl implements IPokemonService{

	private final Logger LOG = LoggerFactory.getLogger(PokemonServiceImpl.class);
	
	private final RestTemplate restTemplate;
	
	@Value("${pokemon-service.base-url}")
	private String baseUrl;
	
	/**
	 * Metodo que inicializa el restTemplateBuilder
	 * @param restTemplateBuilder
	 */
	public PokemonServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Pokemon getPokemon(String name) {
		
		ResponseEntity<Pokemon> response = this.restTemplate.exchange(baseUrl+"{name}", HttpMethod.GET, new HttpEntity<Object>(getHeaders()),Pokemon.class,name);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			return response.getBody();
		}
		LOG.info("El pokemon no existe o hubo un error de comunicacion: "+name);
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<LocationArea> getLocationAreaEncounters(String name) {
		ResponseEntity<LocationArea[]> response = this.restTemplate.getForEntity(
					baseUrl+"{name}/encounters",
				  LocationArea[].class,name);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			return Arrays.asList(response.getBody());
		}
		LOG.info("El pokemon no existe o hubo un error de comunicacion: "+name);
		return null;
	}

	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        return headers;
	}
	
}
