package com.bancaya.prueba.service;

import java.util.List;

import com.bancaya.prueba.model.rest.Pokemon;
import com.bancaya.prueba.model.rest.encounter.LocationArea;

/**
 * Interface para las peticiones rest de los pokemon
 * @author jesus
 *
 */
public interface IPokemonService {

	/**
	 * Obtiene el pokemon por nombre
	 * @param name
	 * @return
	 */
	Pokemon getPokemon(String name);
	
	/**
	 * Obtienen las localizaciones del pokemon
	 * @param name
	 * @return
	 */
	List<LocationArea> getLocationAreaEncounters(String name);
	
}
