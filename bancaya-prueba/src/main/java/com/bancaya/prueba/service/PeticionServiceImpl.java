package com.bancaya.prueba.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancaya.prueba.model.Metodo;
import com.bancaya.prueba.model.Peticion;
import com.bancaya.prueba.repository.PeticionRepository;
import com.bancaya.prueba.utils.PokemonUtils;

/**
 * Servicio para las peticiones realizadas
 * @author jesus
 *
 */
@Service
public class PeticionServiceImpl implements IPeticionService{
	
	@Autowired
	private PeticionRepository peticionRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Peticion guardarPeticion(Metodo metodo) {
		Peticion p = new Peticion();
		p.setMetodo(metodo);
		p.setIpOrigen(PokemonUtils.getClientIpAddress(request));
		p = peticionRepository.save(p);
		return p;
	}

}
