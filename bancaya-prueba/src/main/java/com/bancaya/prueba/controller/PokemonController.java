package com.bancaya.prueba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancaya.prueba.model.rest.Pokemon;
import com.bancaya.prueba.model.rest.encounter.LocationArea;
import com.bancaya.prueba.service.IPokemonService;

/**
 * No es solicitado, se uso para pruebas
 * @author jesus
 *
 */
@RequestMapping("/pokemon")
@RestController
public class PokemonController {

	@Autowired
	private IPokemonService pokemonService;
	
	/**
	 * Metodo REST que obtiene el pokemon por el nombre
	 * @param name
	 * @return
	 */
	@GetMapping("/{name}")
	public Pokemon getPokemonByName(@PathVariable String name) {
		return pokemonService.getPokemon(name);
	}
	
	/**
	 * Metodo REST que obtiene las locaciones del pokemon por el nombre
	 * @param name
	 * @return
	 */
	@GetMapping("/{name}/encounters")
	public List<LocationArea> getPokemonLocationsByName(@PathVariable String name) {
		return pokemonService.getLocationAreaEncounters(name);
	}
	
}
