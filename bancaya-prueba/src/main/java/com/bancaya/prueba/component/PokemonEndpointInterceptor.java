package com.bancaya.prueba.component;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.MethodEndpoint;

import com.bancaya.prueba.model.Metodo;
import com.bancaya.prueba.model.Peticion;
import com.bancaya.prueba.service.IPeticionService;


/**
 * Interceptor para los mensajes SOAP, guarda las peticiones que se realizan
 * @author jesus
 *
 */
@Component
public class PokemonEndpointInterceptor implements EndpointInterceptor {

	private final Logger LOG = LoggerFactory.getLogger(PokemonEndpointInterceptor.class);
	
	@Autowired
	private IPeticionService iPeticionService;
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
		LOG.info("Endpoint Request Handling");
		if(endpoint instanceof MethodEndpoint) {
			MethodEndpoint mep = (MethodEndpoint) endpoint;
			Metodo metodo = Metodo.get(mep.getMethod().getName());
			Peticion p = iPeticionService.guardarPeticion(metodo);
			LOG.info("Se guardo una peticion: "+p);
		}		
		return true;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
		LOG.info("Endpoint Response Handling");
		return true;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
		LOG.info("Endpoint Exception Handling");
		return true;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
		LOG.info("Execute code after completion");
	}
	
}
