package com.bancaya.prueba.utils;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;

import com.bancaya.prueba.model.rest.Ability;
import com.bancaya.prueba.model.rest.Item;
import com.bancaya.prueba.model.rest.ItemVersion;
import com.bancaya.prueba.model.rest.NameAndUrl;
import com.bancaya.prueba.model.rest.encounter.EncounterDetail;
import com.bancaya.prueba.model.rest.encounter.EncounterVersion;
import com.bancaya.prueba.model.rest.encounter.LocationArea;

import io.spring.guides.gs_producing_web_service.AbilitySOAP;
import io.spring.guides.gs_producing_web_service.ConditionValueSOAP;
import io.spring.guides.gs_producing_web_service.EncounterDetailSOAP;
import io.spring.guides.gs_producing_web_service.EncounterVersionSOAP;
import io.spring.guides.gs_producing_web_service.ItemSOAP;
import io.spring.guides.gs_producing_web_service.ItemVersionSOAP;
import io.spring.guides.gs_producing_web_service.LocationAreaSOAP;

/**
 * Clase de utilidades para el aplicativo
 * @author jesus
 *
 */
public class PokemonUtils {

	/**
	 * Transforma una habilidad a su version SOAP
	 * @param a
	 * @return
	 */
	public static AbilitySOAP abilityToSOAP(Ability a) {
		AbilitySOAP as = new AbilitySOAP();
		as.setIsHidden(a.getIsHidden());
		as.setName(a.getAbility().getName());
		as.setSlot(BigInteger.valueOf(a.getSlot()));
		as.setUrl(a.getAbility().getUrl());
		return as;
	}
	
	/**
	 * Transforma un item a su version SOAP
	 * @param i
	 * @return
	 */
	public static ItemSOAP itemToSOAP(Item i) {
		ItemSOAP is = new ItemSOAP();
		is.setName(i.getItem().getName());
		is.setUrl(i.getItem().getUrl());
		for(ItemVersion iv :i.getVersionDetails()) {
			is.getVersionDetail().add(itemVersionToSOAP(iv));
		}
		return is;
	}
	
	/**
	 * Transforma un item version a su version SOAP
	 * @param iv
	 * @return
	 */
	public static ItemVersionSOAP itemVersionToSOAP(ItemVersion iv) {
		ItemVersionSOAP ivs = new ItemVersionSOAP();
		ivs.setName(iv.getVersion().getName());
		ivs.setRarity(BigInteger.valueOf(iv.getRarity()));
		ivs.setUrl(iv.getVersion().getUrl());
		return ivs;
	}
	
	/**
	 * Transforma una location area a su version SOAP
	 * @param la
	 * @return
	 */
	public static LocationAreaSOAP locationAreaToSOAP(LocationArea la) {
		LocationAreaSOAP las = new LocationAreaSOAP();
		las.setName(la.getLocationArea().getName());
		las.setUrl(la.getLocationArea().getUrl());
		for(EncounterVersion ev : la.getVersionDetails()) {
			las.getVersionDetails().add(encounterVersionToSOAP(ev));
		}
		return las;
	}
	
	/**
	 * Transforma un encounter version a su version SOAP
	 * @param ev
	 * @return
	 */
	public static EncounterVersionSOAP encounterVersionToSOAP(EncounterVersion ev) {
		EncounterVersionSOAP evs = new EncounterVersionSOAP();
		evs.setMaxChance(BigInteger.valueOf(ev.getMaxChance()));
		evs.setName(ev.getVersion().getName());
		evs.setUrl(ev.getVersion().getUrl());
		for(EncounterDetail ed : ev.getEncounterDetails()) {
			evs.getEncounterDetails().add(encounterDetailToSOAP(ed));
		}
		return evs;
	}
	
	/**
	 * Transforma un encounter detail a su version SOAP
	 * @param ed
	 * @return
	 */
	public static EncounterDetailSOAP encounterDetailToSOAP(EncounterDetail ed) {
		EncounterDetailSOAP eds = new EncounterDetailSOAP();
		eds.setChance(BigInteger.valueOf(ed.getChance()));
		eds.setMaxLevel(BigInteger.valueOf(ed.getMaxLevel()));
		eds.setMinLevel(BigInteger.valueOf(ed.getMinLevel()));
		eds.setName(ed.getMethod().getName());
		eds.setUrl(ed.getMethod().getUrl());
		for(NameAndUrl nau : ed.getConditionValues()) {
			ConditionValueSOAP cvs = new ConditionValueSOAP();
			cvs.setName(nau.getName());
			cvs.setUrl(nau.getUrl());
			eds.getConditionValues().add(cvs);
		}
		return eds;
	}
	
	private static final String[] IP_HEADER_CANDIDATES = {
		        "X-Forwarded-For",
		        "Proxy-Client-IP",
		        "WL-Proxy-Client-IP",
		        "HTTP_X_FORWARDED_FOR",
		        "HTTP_X_FORWARDED",
		        "HTTP_X_CLUSTER_CLIENT_IP",
		        "HTTP_CLIENT_IP",
		        "HTTP_FORWARDED_FOR",
		        "HTTP_FORWARDED",
		        "HTTP_VIA",
		        "REMOTE_ADDR" };

	/**
	 * Obtiene la ip del cliente desde un request
	 * @param a
	 * @return
	 */
	public static String getClientIpAddress(HttpServletRequest request) {
		    for (String header : IP_HEADER_CANDIDATES) {
		        String ip = request.getHeader(header);
		        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
		            return ip;
		        }
		    }
		    return request.getRemoteAddr();
	}
}
